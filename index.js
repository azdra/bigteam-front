import 'react-native-gesture-handler';
import {registerRootComponent} from 'expo';
import Application from './src/Application';

registerRootComponent(Application);
