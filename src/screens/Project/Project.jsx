import * as React from 'react';
import {TaskCard} from '../../components/TaskCard/TaskCard';
import {View, Text} from 'react-native';
import {colors} from '../../config/color';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import {useNavigation} from '@react-navigation/native';
import {useQuery, useQueryClient} from 'react-query';
import CreateTaskForm from '../../components/TaskCard/CreateTaskForm';
import {useState} from 'react';

const Container = styled.View`
  flex: 1;
`;

const Content = styled.View`
  flex: 1;
`;

const Overview = styled.View`
  margin-top: 10px;
`;

const Title = styled.Text`
  font-size: 24px;
  font-weight: bold;
`;

const Subtitle = styled.Text`
  font-size: 17px;
  font-weight: bold;
`;

const Button = styled.Pressable`
  align-items: center;
  justify-content: center;
  padding: 12px;
  background-color: ${colors.darkblue};
  border-radius: 6px;
  box-shadow: 3px 3px 3px #d3d3d3;
  margin: 20px;
`;

const ButtonText = styled.Text`
  font-size: 16px;
  line-height: 21px;
  font-weight: bold;
  letter-spacing: 0.25px;
  color: white;
`;

const ScrollContent = styled.ScrollView`
  margin: 10px;
  padding: 10px;
`;

const Project = ({route}) => {
  const {id} = route.params;
  const navigation = useNavigation();
  const queryClient = useQueryClient();

  const [showAddTask, setShowAddTask] = useState(false);

  const {data: project, isLoading} = useQuery(`/projects/${id}`);

  const handleTaskSubmit = () => {
    queryClient.invalidateQueries(`/projects/${id}`);
    setShowAddTask(false);
  };

  return (
    <>
      {isLoading ? (
        <Text>Page loading...</Text>
      ) : (
        <Container>
          <ScrollContent>
            <Title>{project.name}</Title>
            <Overview>
              <Subtitle>{project.description}</Subtitle>
              <Overview>
                <Text>- begin date</Text>
                <Text>- deadline</Text>
                <Text>- total time left/total allotted time</Text>
              </Overview>
            </Overview>
            {
              project.tasks && (
                <Content>
                  {
                    project.tasks.map((task) => (
                      <TaskCard
                        key={task.id}
                        navigation={navigation}
                        task={task}
                      />
                    ))
                  }
                </Content>
              )
            }
          </ScrollContent>
          {showAddTask && (
            <CreateTaskForm
              onClose={() => setShowAddTask(false)}
              onSubmit={handleTaskSubmit}
              project={project && project}
            />
          )}
          <View>
            <Button onPress={() => setShowAddTask(!showAddTask)}>
              <ButtonText>Add a step</ButtonText>
            </Button>
          </View>
        </Container>
      )}
    </>
  );
};

Project.propTypes = {
  route: PropTypes.object,
};

export default Project;
