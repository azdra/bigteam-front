import React, {useState} from 'react';
import {View} from 'react-native';
import {ProjectCard} from '../../components/ProjectCard/ProjectCard';
import {colors} from '../../config/color';
import styled from 'styled-components/native';
import PropTypes from 'prop-types';
import ProjectForm from '../../components/ProjectCard/ProjectCardForm';
import {useQuery, useQueryClient} from 'react-query';
import {ScrollView} from 'react-native-gesture-handler';

const Content = styled(ScrollView)`
  flex: 1;
`;

const Button = styled.Pressable`
  align-items: center;
  justify-content: center;
  padding: 12px;
  background-color: ${colors.darkblue};
  border-radius: 6px;
  box-shadow: 3px 3px 3px #d3d3d3;
  margin: 10px;
`;

const ButtonAction = styled.Text`
  font-size: 16px;
  line-height: 21px;
  font-weight: 700;
  letter-spacing: 0.25px;
  color: #fff;
`;

const Home = (props) => {
  const queryClient = useQueryClient();

  const [showAddProject, setShowAddProject] = useState(false);

  const {data: projects} = useQuery('/projects');

  const handleProjectSubmit = () => {
    queryClient.invalidateQueries('/projects');
    setShowAddProject(false);
  };

  return (
    <View style={{flex: 1}}>
      {projects && (
        <Content>
          {projects.map((project) => (
            <ProjectCard
              key={project.id}
              navigation={props.navigation}
              project={project}
            />
          ))}
        </Content>
      )}

      {
        showAddProject && <ProjectForm
          onClose={() => setShowAddProject(false)}
          onSubmit={handleProjectSubmit}
        />
      }

      <View>
        <Button onPress={() => setShowAddProject(!showAddProject)}>
          <ButtonAction>
            {showAddProject ? 'Submit' : 'Add a project'}
          </ButtonAction>
        </Button>
      </View>
    </View>
  );
};

Home.propTypes = {
  navigation: PropTypes.object,
};

export default Home;
