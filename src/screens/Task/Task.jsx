import * as React from 'react';
import {Text, View} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import {format} from 'date-fns';
import {useQuery} from 'react-query';
import {useEffect, useState} from 'react';
import {colors} from '../../config/color';
import axios from 'axios';
import {api_url} from '../../Application';
import {Timer} from '../../components/Timer';

const Container = styled.View`
  flex: 1;
`;

const Overview = styled.View`
  margin-top: 10px;
`;

const Title = styled.Text`
  font-size: 24px;
  font-weight: bold;
`;

const Subtitle = styled.Text`
  font-size: 17px;
  font-weight: bold;
`;

const ScrollContent = styled.ScrollView`
  margin: 10px;
  padding: 10px;
`;

const TaskStatus = styled.View`
  flex: 1;
`;

const Button = styled.Pressable`
  align-items: center;
  justify-content: center;
  padding: 12px;
  background-color: ${colors.darkblue};
  border-radius: 6px;
  box-shadow: 3px 3px 3px #d3d3d3;
`;

const ButtonText = styled.Text`
  font-size: 16px;
  line-height: 21px;
  font-weight: bold;
  letter-spacing: 0.25px;
  color: white;
`;

export const taskKeyToName = {
  to_do: 'To Do',
  in_progress: 'In Progress',
  to_test: 'To Test',
  to_end: 'Finish',
};

const Task = ({route}) => {
  const {id} = route.params;
  const {data: task, isLoading} = useQuery(`/tasks/${id}`);
  const [selectedValue, setSelectedValue] = useState('temporibus');

  useEffect(() => {
    if (task) setSelectedValue(task.status);
  }, [isLoading, task]);

  const submitUpdate = () => {
    console.debug('couc');
    axios({
      method: 'PUT',
      url: api_url+`/tasks/${id}`,
      data: {
        status: selectedValue,
      },
    });
  };

  return (
    <>
      {isLoading ? (
        <Text>Page loading...</Text>
      ) : (
        <Container>
          <ScrollContent>
            <Title>{task.name}</Title>
            <Overview>
              <Subtitle>Task overview</Subtitle>
              {
                task.created_at &&
                <Text>
                  - Created at : {
                    format(new Date(task.created_at), 'dd.MM.yyyy')
                  }
                </Text>
              }
              <Text>- Status : {task.status}</Text>
              <Text>- Expected Duration : {task.expected_duration}</Text>
              <Text>- Actual Duration : {task.actual_duration}</Text>
              <View>
                <Text>- Status</Text>
                <View>
                  <RNPickerSelect
                    style={{
                      placeholder: {color: 'dark'},
                      inputIOS: {color: 'dark'},
                      inputAndroid: {color: 'dark'},
                    }}
                    value={selectedValue}
                    onValueChange={
                      (itemValue, itemIndex) => setSelectedValue(itemValue)
                    }
                    items={[
                      {label: 'To Do', value: 'to_do'},
                      {label: 'In Progress', value: 'in_progress'},
                      {label: 'To Test', value: 'to_test'},
                      {label: 'Finish', value: 'to_end'},
                    ]}
                  />
                </View>
              </View>
              <View>
                <Button onPress={submitUpdate}>
                  <ButtonText>Update status</ButtonText>
                </Button>
              </View>
            </Overview>
            <Timer task={task}/>
          </ScrollContent>
        </Container>
      )}
    </>
  );
};

Task.propTypes = {
  route: PropTypes.object,
  task: {
    name: PropTypes.string,
    created_at: PropTypes.string,
  },
};

export default Task;
