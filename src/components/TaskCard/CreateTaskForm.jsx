import React from 'react';
import styled from 'styled-components/native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {Formik} from 'formik';
import * as yup from 'yup';
import {Text} from 'react-native';
import {colors} from '../../config/color';
import {useMutation} from 'react-query';
import axios from 'axios';
import PropTypes from 'prop-types';
import Home from '../../screens/Home/Home';

const Card = styled.View`
  margin: 10px;
  padding: 16px;
  border-radius: 6px;
  background-color: #fff;
  box-shadow: 3px 3px 3px #d3d3d3;
`;

const CloseIcon = styled.Pressable`
  display: flex;
  align-items: flex-end;
  padding: 5px 10px;
`;

const Label = styled.Text`
  margin: 15px 0 5px;
  margin-bottom: 5px;
`;

const Input = styled.TextInput`
  height: 40px;
  border: 2px solid #eeeded;
  border-radius: 4px;
  padding: 5px;
`;

const ErrorMessage = styled.Text`
  color: #dc1c13;
`;

const SubmitButton = styled.Pressable`
  background-color: ${colors.gray};
`;

const initialValues = {
  name: '',
  description: '',
  expected_duration: '',
};

const validationSchema = yup.object().shape({
  name: yup.string().required('This field is required'),
  description: yup.string().required('This field is required'),
  expected_duration: yup.number().typeError('Must be a number').required('This field is required'),
});

const CreateTaskForm = ({onClose, onSubmit, project}) => {
  const createTask = (task) => {
    axios.post('https://project-expo-back.herokuapp.com/tasks', {
      ...task,
      project: project.id,
    });
  };

  const {mutate: mutateTask} = useMutation(createTask);

  const onFormSubmit = (values) => {
    mutateTask(values);
    onSubmit();
  };

  return (
    <Card>
      <CloseIcon onPress={onClose}>
        <FontAwesomeIcon icon="times" />
      </CloseIcon>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onFormSubmit}
        validateOnChange={false}
      >
        {({handleChange, handleSubmit, values, errors}) => (
          <>
            <Label htmlFor="name">Task name :</Label>
            <Input
              id="name"
              name="name"
              placeholder="your task name"
              onChangeText={handleChange('name')}
              value={values.name}
            />
            {errors.name && <ErrorMessage>{errors.name}</ErrorMessage>}

            <Label htmlFor="description">Description :</Label>
            <Input
              id="description"
              name="description"
              placeholder="your task description"
              onChangeText={handleChange('description')}
              value={values.description}
            />

            {
              errors.description && <ErrorMessage>
                {errors.description}
              </ErrorMessage>
            }

            <Label htmlFor="expected_duration">Expected duration :</Label>
            <Input
              id="expected_duration"
              name="expected_duration"
              placeholder="your task duration in minutes"
              onChangeText={handleChange('expected_duration')}
              value={values.expected_duration}
            />

            {
              errors.expected_duration && <ErrorMessage>
                {errors.expected_duration}
              </ErrorMessage>
            }

            <SubmitButton onPress={handleSubmit}>
              <Text>Submit</Text>
            </SubmitButton>
          </>
        )}
      </Formik>
    </Card>
  );
};

CreateTaskForm.propTypes = {
  onClose: PropTypes.func,
  onSubmit: PropTypes.func,
  project: PropTypes.object,
};

export default CreateTaskForm;
