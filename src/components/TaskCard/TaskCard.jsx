import React from 'react';
import styled from 'styled-components/native';
import {View, Pressable} from 'react-native';
import {format} from 'date-fns';
import PropTypes from 'prop-types';
import {taskKeyToName} from '../../screens/Task/Task';

const Card = styled.View`
  margin: 10px 0px;
  padding: 16px;
  border-radius: 6px;
  background-color: #fff;
  box-shadow: 3px 3px 3px #d3d3d3;
`;

const Name = styled.Text`
  font-size: 24px;
  font-weight: 700;
  margin-bottom: 10px;
`;

const Info = styled.Text`
  font-size: 14px;
  color: #777;
`;

export const TaskCard = ({navigation, task}) => {
  const {name, id, created_at, status} = task;

  const taskClick = () => {
    navigation.navigate('TaskDetail', {
      id,
    });
  };

  return (
    <Pressable onPress={taskClick}>
      <Card>
        <Name>{name}</Name>
        <View>
          <Info>Created on : {format(new Date(created_at), 'dd.MM.yyyy')}</Info>
          <Info>Status : {taskKeyToName[status] ?? 'No status'}</Info>
        </View>
      </Card>
    </Pressable>
  );
};

TaskCard.propTypes = {
  task: PropTypes.object,
  navigation: PropTypes.object,
};
