import React, {useState, useRef} from 'react';
import styled from 'styled-components/native';
import {colors} from '../config/color';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import PropTypes from 'prop-types';
import axios from 'axios';
import {useMutation, useQueryClient} from 'react-query';

const TimerContainer = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
  margin-top: 40px;
  padding: 10px;
  background-color: ${colors.darkblue};
  border-radius: 6px;
`;

const TimerText = styled.Text`
  font-size: 16px;
  line-height: 21px;
  font-weight: 700;
  letter-spacing: 0.25px;
  color: #fff;
`;

const ButtonAction = styled.Pressable`
  flex-direction: column;
  justify-content: center;
  align-items: center;
  border: solid 1px ${colors.orange};
  border-radius: 50px;
  background-color: ${colors.orange};
  color: #fff;
  padding: 10px;
`;

export const Timer = ({task}) => {
  const queryClient = useQueryClient();
  const mutation = useMutation((data) => axios.put(`https://project-expo-back.herokuapp.com/tasks/${task.id}`, data), {
    onSuccess: () => {
      queryClient.invalidateQueries(`/tasks/${task.id}`);
    },
  });

  const [actualDuration, setActualDuration] = useState(parseInt(task.actual_duration) * 60 || 0);
  const displayedActualDuration = new Date(actualDuration * 1000).toISOString().substr(11, 8);

  const expectedDuration = parseInt(task.expected_duration) * 60 || 0;
  const displayedExpectedDuration = new Date(expectedDuration * 1000).toISOString().substr(11, 8);

  const [isActive, setIsActive] = useState(false);
  const myInterval = useRef(null);

  const onStart = () => {
    setIsActive(true);
    myInterval.current = setInterval(() => {
      setActualDuration((actualDuration) => actualDuration + 1);
    }, 1000);
  };

  const onPause = () => {
    setIsActive(false);
    clearInterval(myInterval.current);
    mutation.mutate({
      actual_duration: Math.round(actualDuration / 60),
    });
  };

  return (
    <TimerContainer>
      <TimerText>
        {displayedActualDuration} / {displayedExpectedDuration}
      </TimerText>

      <ButtonAction onPress={isActive ? onPause : onStart}>
        <FontAwesomeIcon icon={isActive ? 'pause' : 'play'} />
      </ButtonAction>
    </TimerContainer>
  );
};

Timer.propTypes = {
  task: PropTypes.object,
};
