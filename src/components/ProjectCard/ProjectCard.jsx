import React from 'react';
import styled from 'styled-components/native';
import {View, Pressable} from 'react-native';
import {format} from 'date-fns';
import PropTypes from 'prop-types';

const Card = styled.View`
  margin: 10px;
  padding: 16px;
  border-radius: 6px;
  background-color: #fff;
  box-shadow: 3px 3px 3px #d3d3d3;
`;

const Name = styled.Text`
  font-size: 24px;
  font-weight: 700;
  margin-bottom: 10px;
`;

const Info = styled.Text`
  font-size: 14px;
  color: #777;
`;

export const ProjectCard = ({navigation, project}) => {
  const {name, id, created_at, tasks} = project;

  const projectClick = () => {
    navigation.navigate('ProjectDetail', {
      id,
    });
  };

  return (
    <Pressable onPress={projectClick}>
      <Card>
        <Name>{name}</Name>
        <View>
          <Info>Created at : {format(new Date(created_at), 'dd.MM.yyyy')}</Info>
          <Info>Number of tasks : {tasks.length}</Info>
        </View>
      </Card>
    </Pressable>
  );
};

ProjectCard.propTypes = {
  project: PropTypes.object,
  navigation: PropTypes.object,
};
