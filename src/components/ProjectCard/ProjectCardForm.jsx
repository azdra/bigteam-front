import React from 'react';
import styled from 'styled-components/native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {Formik} from 'formik';
import * as yup from 'yup';
import {Text} from 'react-native';
import {colors} from '../../config/color';
import {useMutation} from 'react-query';
import axios from 'axios';
import PropTypes from 'prop-types';

const Card = styled.View`
  margin: 10px;
  padding: 16px;
  border-radius: 6px;
  background-color: #fff;
  box-shadow: 3px 3px 3px #d3d3d3;
`;

const CloseIcon = styled.Pressable`
  display: flex;
  align-items: flex-end;
  padding: 5px 10px;
`;

const Label = styled.Text`
  margin: 15px 0 5px;
  margin-bottom: 5px;
`;

const Input = styled.TextInput`
  height: 40px;
  border: 2px solid #eeeded;
  border-radius: 4px;
  padding: 5px;
`;

const ErrorMessage = styled.Text`
  color: #dc1c13;
`;

const SubmitButton = styled.Pressable`
  background-color: ${colors.gray};
`;

const initialValues = {
  name: '',
  description: '',
};

const validationSchema = yup.object().shape({
  name: yup.string().required('This field is required'),
  description: yup.string().required('This field is required'),
});

const ProjectForm = ({onClose, onSubmit}) => {
  const createProject = (project) => {
    console.log('PROJECT :', project);
    axios.post('https://project-expo-back.herokuapp.com/projects', {...project});
  };

  const {mutate: mutateProject} = useMutation(createProject);

  const onFormSubmit = (values) => {
    mutateProject(values);
    onSubmit();
  };

  return (
    <Card>
      <CloseIcon onPress={onClose}>
        <FontAwesomeIcon icon="times" />
      </CloseIcon>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onFormSubmit}
        validateOnChange={false}
      >
        {({handleChange, handleSubmit, values, errors}) => (
          <>
            <Label htmlFor="name">Project title :</Label>
            <Input
              id="name"
              name="name"
              placeholder="your project title"
              onChangeText={handleChange('name')}
              value={values.name}
            />
            {errors.name && <ErrorMessage>{errors.name}</ErrorMessage>}

            <Label htmlFor="description">Description :</Label>
            <Input
              id="description"
              name="description"
              placeholder="your project description"
              onChangeText={handleChange('description')}
              value={values.description}
            />

            {
              errors.description && <ErrorMessage>
                {errors.description}
              </ErrorMessage>
            }

            <SubmitButton onPress={handleSubmit}>
              <Text>Submit</Text>
            </SubmitButton>
          </>
        )}
      </Formik>
    </Card>
  );
};

ProjectForm.propTypes = {
  onClose: PropTypes,
  onSubmit: PropTypes.object,
};

export default ProjectForm;
