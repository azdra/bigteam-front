import React from 'react';
import styled from 'styled-components/native';

const Tagline = styled.Text`
  color: #FFFFFF;
`;

const LogoImg = styled.Image`
  width: 117px;
  height: 50px;
`;

const Homos = styled.View`
  padding: 10px;
`;

export const Logo = () => {
  return (
    <Homos>
      <LogoImg source={require('../logo/logo.png')} />
      <Tagline>Manage like a boss</Tagline>
    </Homos>
  );
};
