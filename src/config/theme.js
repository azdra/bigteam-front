import {colors} from './color';

export const themeProvider = {
  dark: false,
  colors: {
    primary: colors.darkblue,
    background: colors.gray,
    card: 'rgb(255, 255, 255)',
    text: 'rgb(28, 28, 30)',
    border: 'rgb(199, 199, 204)',
    notification: 'rgb(255, 69, 58)',
  },
};
