export const translations = {
  ADD_NEW_PROJECT: 'Add a project',
  ADD_STEP: 'Add a step',
  BEGIN_AT: '- Begin at: {{DATE}}',
  DEADLINE_AT: '- Deadline at: {{DATE}}',
  ALLOTTED_TIME: '- Allotted time: {{TIME}}',
  NUMBER_STEPS: '- Number of steps: {{NUMBER}}',
  YESTERDAY_AT: 'Yesterday at {{TIME}}',
  TODAY_AT: 'Today at {{TIME}}',
  NAV_BACK: 'Go back',
};
