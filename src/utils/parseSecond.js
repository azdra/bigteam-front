// TODO Code pas bo à réfacto si tu as la motiv
/**
 * @return {string}
 * @param {number} s
 */
export const parseSecond = (s) => {
  let seconds = s;
  let hours = Math.floor(seconds / 3600) || '';
  let minutes = Math.floor((seconds - hours * 3600) / 60) || '';

  let str = '';

  if (hours && hours < 10) {
    hours = '0' + hours;
  }

  if (minutes && minutes < 10) {
    minutes = '0' + minutes;
  }

  if (seconds < 10) {
    seconds = '0' + seconds;
  }

  if (hours) str += hours + ':';
  if (minutes) str += minutes + ':';

  let second = seconds % 60;

  if (second === 0) {
    second = second + '0';
  }

  return str + second;
};
