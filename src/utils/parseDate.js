import dayjs from 'dayjs';
import {text} from './text';
import yesterday from 'dayjs/plugin/isYesterday';
import today from 'dayjs/plugin/isToday';
dayjs.extend(yesterday);
dayjs.extend(today);

export const parseDate = (date) => {
  if (dayjs(date).isYesterday() && !dayjs(date).isToday()) {
    return text('YESTERDAY_AT', {
      TIME: dayjs(date).format('HH:mm'),
    });
  }

  if (!dayjs(date).isYesterday() && dayjs(date).isToday()) {
    return text('TODAY_AT', {
      TIME: dayjs(date).format('HH:mm'),
    });
  }

  return dayjs(date).format('DD/MM/YY');
};
