import {translations} from '../config/translations';

export const text = (keyTranslation, options) => {
  let str = translations[keyTranslation];
  if (!str) return keyTranslation;

  if (options && typeof options === 'object') {
    for (const i of Object.keys(options)) {
      const re = new RegExp(`{{${i}}}`, 'gi');
      if (re) str = str.replace(re, options[i]);
    }
  }

  return str;
};
