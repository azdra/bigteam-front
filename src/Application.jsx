import React from 'react';
import Home from './screens/Home/Home';
import Project from './screens/Project/Project';
import Task from './screens/Task/Task';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {themeProvider} from './config/theme';
import {colors} from './config/color';
import {Logo} from './components/Logo';
import {library} from '@fortawesome/fontawesome-svg-core';
import {QueryClient, QueryClientProvider} from 'react-query';
import axios from 'axios';
import {
  faTimes,
  faCalendar,
  faPlay,
  faPause,
  faClock,
} from '@fortawesome/free-solid-svg-icons';

library.add(faTimes, faCalendar, faPlay, faPause, faClock);
const Stack = createNativeStackNavigator();

export const api_url = 'https://project-expo-back.herokuapp.com';

const Application = () => {
  return (
    <QueryClientProvider
      client={
        new QueryClient({
          defaultOptions: {
            queries: {
              queryFn: async ({queryKey}) => {
                const {data} = await axios.get(`${api_url+queryKey[0]}`);
                return data;
              },
            },
          },
        })
      }
    >
      <SafeAreaProvider>
        <NavigationContainer theme={themeProvider}>
          <Stack.Navigator
            screenOptions={{
              headerStyle: {
                backgroundColor: colors.darkblue,
              },
              headerTitleAlign: 'center',
              headerTintColor: '#fff',
              headerTitleStyle: {
                fontWeight: 'bold',
                alignSelf: 'center',
              },
            }}
            initialRouteName="Home"
          >
            <Stack.Screen
              name="Home"
              component={Home}
              options={{
                headerTitle: (props) => <Logo {...props} />,
                headerTitleStyle: {
                  textAlign: 'center',
                  flex: 1,
                },
              }}
            />
            <Stack.Screen name="ProjectDetail" component={Project} />
            <Stack.Screen name="TaskDetail" component={Task} />
          </Stack.Navigator>
        </NavigationContainer>
      </SafeAreaProvider>
    </QueryClientProvider>
  );
};

export default Application;
